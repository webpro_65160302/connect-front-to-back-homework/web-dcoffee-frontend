type Gender = 'male' | 'female' | 'others'
type Role = 'manager' | 'staff'
type Employee = {
  id: number
  name: string
  email: string
  tel: string
  password: string
  gender: Gender
  roles: Role[]
}
export type { Gender, Role, Employee }
