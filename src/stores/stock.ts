import { ref } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import stockService from '@/services/stock'
import type { Stock } from '@/types/Stock'

export const useStockStore = defineStore('stock', () => {
  const loadingStore = useLoadingStore()

  const stocks = ref<Stock[]>([])
  const initialStock: Stock = {
    name: '',
    qty: 0,
    priceUnit: 0
  }
  const editedStock = ref<Stock>(JSON.parse(JSON.stringify(initialStock)))

  async function getStock(id: number) {
    loadingStore.doLoad()
    const res = await stockService.getStock(id)
    editedStock.value = res.data
    loadingStore.finish()
  }

  async function getStocks() {
    loadingStore.doLoad()
    const res = await stockService.getStocks()
    stocks.value = res.data
    loadingStore.finish()
  }

  async function saveStock() {
    loadingStore.doLoad()
    const stock = editedStock.value
    if (!stock.id) {
      // Add new
      console.log('Post ' + JSON.stringify(stock))
      const res = await stockService.addStock(stock)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(stock))
      const res = await stockService.updateStock(stock)
    }
    await getStocks()
    loadingStore.finish()
    clearForm()
  }

  async function deleteStock() {
    loadingStore.doLoad()
    const stock = editedStock.value
    const res = await stockService.delStock(stock)
    await getStocks()
    loadingStore.finish()
  }

  function clearForm() {
    editedStock.value = JSON.parse(JSON.stringify(initialStock))
  }

  return { stocks, editedStock, getStocks, getStock, saveStock, deleteStock, clearForm }
})
