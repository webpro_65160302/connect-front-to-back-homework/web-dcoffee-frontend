// import {type  Employee } from "@/types/Employee";
import { computed, nextTick, onMounted } from 'vue'
import { ref } from 'vue'
import type { VForm } from 'vuetify/components'
import type { Member } from '@/types/Member'
import { defineStore } from 'pinia'
import { useReceipStore } from './receipt'

export const useMemberStore = defineStore('members', () => {
  const initialMember: Member = {
    id: -1,
    name: '',
    tel: ''
  }
  const search = ref('')
  const filteredMember = computed(() => {
    return members.value.filter((members) => members.name.includes(search.value))
  })

  const editedMember = ref<Member>(JSON.parse(JSON.stringify(initialMember)))
  const members = ref<Member[]>([])
  const tel = ref('')
  let editedIndex = -1
  let lastId = 6
  const itemsPerPage = ref<any>(5)
  const page = ref(1)
  const pageCount = computed(() => {
    return Math.ceil(members.value.length / itemsPerPage.value)
  })
  const form = ref(false)
  const refForm = ref<VForm | null>(null)
  const dialog = ref(false)

  const dialogDelete = ref(false)

  const loading = ref(false)

  const headers = [
    {
      title: 'ID',
      key: 'id',
      value: 'id'
    },
    {
      title: 'Name',
      key: 'name',
      value: 'name'
    },
    {
      title: 'Tel',
      key: 'tel',
      value: 'tel'
    },
    { title: '', key: 'actions', sortable: false }
  ]

  function initialize() {
    members.value = [
      {
        id: 1,
        name: 'manager',
        tel: '0987654321'
      },
      {
        id: 2,
        name: 'kanoknapas',
        tel: '0987653466'
      },
      {
        id: 3,
        name: 'staff',
        tel: '0987634876'
      },
      {
        id: 4,
        name: 'top',
        tel: '0987947455'
      },
      {
        id: 5,
        name: 'staff3',
        tel: '123'
      }
    ]
  }
  onMounted(() => {
    initialize()
  })

  function closeDialog() {
    dialog.value = false
    nextTick(() => {
      editedMember.value = Object.assign({}, initialMember)
      editedIndex = -1
    })
  }

  async function save() {
    const { valid } = await refForm.value!.validate()

    if (!valid) return
    if (editedIndex > -1) {
      Object.assign(members.value[editedIndex], editedMember.value)
    } else {
      editedMember.value.id = lastId++
      members.value.push(editedMember.value)
    }
    closeDialog()
  }

  function onSubmit() {}

  function editItem(item: Member) {
    editedIndex = members.value.indexOf(item)
    editedMember.value = Object.assign({}, item)
    dialog.value = true
  }

  function deleteItem(item: Member) {
    editedIndex = members.value.indexOf(item)
    editedMember.value = Object.assign({}, item)
    dialogDelete.value = true
  }

  function deleteItemConfirm() {
    members.value.splice(editedIndex, 1)
    closeDelete()
    editedIndex = -1
  }

  function closeDelete() {
    dialogDelete.value = false
    nextTick(() => {
      editedMember.value = Object.assign({}, initialMember)
    })
  }
  const receipt = useReceipStore()

  const currentMember = ref<Member | null>()
  const searchMember = (tel: string) => {
    const index = members.value.findIndex((item) => item.tel === tel)
    if (index < 0) {
      currentMember.value = null
    }
    currentMember.value = members.value[index]
    receipt.updateCurrentMember(currentMember.value)
  }
  function clear() {
    currentMember.value = null
    tel.value = ''
  }
  function clearSearch() {
    tel.value = ''
  }

  return {
    tel,
    form,
    loading,
    itemsPerPage,
    page,
    pageCount,
    filteredMember,
    headers,
    currentMember,
    editedMember,
    dialog,
    search,
    dialogDelete,
    save,
    onSubmit,
    editItem,
    deleteItem,
    deleteItemConfirm,
    searchMember,
    clear,
    closeDialog,
    closeDelete,
    initialize,
    clearSearch
  }
})
